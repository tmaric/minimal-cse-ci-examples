#include<vector>
#include<algorithm>
#include<numeric> 
#include<iostream>
#include<cmath>
#include<iomanip>
#include<fstream>
#include<string>

using std::ofstream;
using std::pow;

template <typename Container, typename OStream> 
void print(
    Container const &c, 
    char sep = ' ', 
    OStream& os = std::cout
) 
{
  for (const auto &e : c)
    os << e << sep;
}

class polynomial
{
    std::vector<double> coeffs_; 
    
    public: 

        polynomial(std::initializer_list<double>&& lcoeffs)
            :
                coeffs_(std::move(lcoeffs))
        {};

        const std::vector<double>& coeffs() const { return coeffs_; }

        double operator()(double x) const
        {
            double result = 0;
            std::size_t exp = 0;  
            for (const auto& coeff: coeffs_)
            {
                result += coeff*std::pow(x,exp);
                ++exp;
            }
            return result;
        }
        
        double fx(double x) const
        {
            return this->operator()(x);
        }
        
        double dfdx(double x) const
        {
            double result = 0; 

            for (std::size_t exp = 1; exp < coeffs_.size(); ++exp)
              result += exp * coeffs_[exp] * std::pow(x, exp - 1);

            return result;
        }

        double d2fdx2(double x) const
        {
            double result = 0; 

            for (std::size_t exp = 2; exp < coeffs_.size(); ++exp)
              result += exp * (exp-1) * coeffs_[exp] * std::pow(x, exp - 2);

            return result;
        }
};

template<typename Container> 
Container dfdx_cds(Container const& x, Container const& fx)
{
    Container dfdx_cds(x.size(), 0);
    const auto end = x.size() - 1;
    for (decltype(x.size()) i = 1; i < end; ++i)
    {
        dfdx_cds[i] = (fx[i+1] - fx[i-1]) / (x[i+1] - x[i-1]);
    }
    // 2nd-order forward difference
    dfdx_cds[0] = (-3*fx[0] + 4*fx[1] - fx[2]) / 
        (2*(x[1] - x[0]));
    // 2nd-order backward difference
    dfdx_cds[end] = (3*fx[end] - 4*fx[end-1] + fx[end-2]) / 
        (2*(x[end] - x[end-1]));
    
    return dfdx_cds;
}

template<typename Container> 
Container d2fdx2_cds(Container const& x, Container const& fx)
{
    Container dfdx_cds(x.size(), 0);
    const auto end = x.size() - 1;
    for (decltype(x.size()) i = 1; i < end; ++i)
    {
        dfdx_cds[i] = (fx[i+1] - 2*fx[i] + fx[i-1]) / 
            pow(x[i+1] - x[i], 2);
    }
    // 2nd-order forward difference
    dfdx_cds[0] = (fx[2] - 2*fx[1] + fx[0]) / 
        pow(x[1] - x[0],2);
    // 2nd-order backward difference
    dfdx_cds[end] = (fx[end] - 2*fx[end-1]  + fx[end-2]) / 
        pow(x[end] - x[end-1], 2);
    
    return dfdx_cds;
}

using namespace std;

int main(int argc, char* argv[])
{
    int npoints = 101;
    
    if (argc > 1)
        npoints = stoi(argv[1]) + 1; 

    std::vector<double> xvalues(npoints);  
    std::generate(
        xvalues.begin(), xvalues.end(), 
        [n = 0, npoints] () mutable 
        { 
            return double(n++) / (npoints-1); 
        }
    ); 
    
    polynomial poly({-1,2,-3,4});
    
    std::vector<double> fx (npoints);
    std::vector<double> dfdx (npoints);
    std::vector<double> d2fdx2 (npoints);
    
    for (decltype(xvalues.size()) i = 0; i < xvalues.size(); ++i)
    {
        fx[i]= poly(xvalues[i]);
        dfdx[i] = poly.dfdx(xvalues[i]);
        d2fdx2[i] = poly.d2fdx2(xvalues[i]);
    }
    
    auto dfdxcds = dfdx_cds(xvalues, fx);
    auto d2fdx2cds = d2fdx2_cds(xvalues, fx);

    ofstream polydata("poly-data.csv");
    polydata << "#{poly_coeffs : [";
    print(poly.coeffs(), ' ', polydata);
    polydata << "]";
    polydata << ", npoints : " << npoints - 1 << "}\n";
    polydata << "X,FX,DFDX,DFDX_CDS,D2FDX2,D2FDX2_CDS\n";

    for (decltype(xvalues.size()) i = 0; i < xvalues.size(); ++i)
    {
        polydata << std::setprecision(16) 
            << xvalues[i] << ","
            << fx[i] << "," 
            << dfdx[i] << ","
            << dfdxcds[i] << ","
            << d2fdx2[i] << ","
            << d2fdx2cds[i] << "\n"; 
    }

    return 0;
}